//
//  TabViewButton.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import UIKit

final class TabViewButton: UIButton {
    //select backgroundColor
    var selectBackgroundColor: UIColor?
    var nonSelectBackgroundColor: UIColor?
    //select borderColor
    var selectedBorderColor: UIColor?
    var nonSelectedBorderColor: UIColor?
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.layer.backgroundColor = selectBackgroundColor?.cgColor
                self.layer.borderColor = selectedBorderColor?.cgColor
            } else {
                self.layer.backgroundColor = nonSelectBackgroundColor?.cgColor
                self.layer.borderColor = nonSelectBackgroundColor?.cgColor
            }
        }
    }
}
