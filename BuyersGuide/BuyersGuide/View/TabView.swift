//
//  TabView.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import UIKit

protocol TabViewDelegate: class {
    func tabView(_ tabView: TabView, didSelectAt index: Int)
}

final class TabView: UIView {
    private var buttons: [UIButton] = []
    private var borders: [UIView] = []

    private let defaultTextColor = UIColor.lightGray
    private let selectedTextColor = UIColor.black
    private let defaultBackgroundColor = UIColor.white
    private let selectedBackgroundColor = UIColor.white

    private let borderHeight: CGFloat = 2
    private let defaultBorderColor = UIColor.lightGray
    private let selectedBorderColor = UIColor.black

    weak var delegate: TabViewDelegate?

    var tabCount: Int { return buttons.count }
    var selectedIndex: Int {
        return buttons.first(where: { $0.isSelected })?.tag ?? 0
    }

    convenience init(titles: [String]) {
        self.init()
        setup(titles: titles)
    }

    private func setup(titles: [String]) {

        backgroundColor = defaultBackgroundColor

        let rootStackView = UIStackView()
        rootStackView.axis = .horizontal
        rootStackView.distribution = .fillEqually
        rootStackView.frame = bounds
        rootStackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(rootStackView)

        buttons = titles.enumerated().map { createButton(title: $0.element, index: $0.offset) }
        borders = buttons.map { createBorder(index: $0.tag) }

        let lastIndex = titles.count - 1
        let menuViews: [UIView] = buttons.enumerated().map {
            $0.offset == lastIndex ? $0.element : createSeparatorView(with: $0.element)
        }


        zip(menuViews, borders).map {
            let stackView = UIStackView(arrangedSubviews: [$0, $1])
            stackView.axis = .vertical
            return stackView
            }
            .forEach { rootStackView.addArrangedSubview($0) }


        zip(buttons, buttons.dropFirst()).forEach {
            $0.0.heightAnchor.constraint(equalTo: $0.1.heightAnchor).isActive = true
        }

        setActive(at: 0)
    }

    private func createButton(title: String, index: Int) -> UIButton {
        let button = TabViewButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(defaultTextColor, for: .normal)
        button.setTitleColor(selectedTextColor, for: .selected)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.selectBackgroundColor = selectedBackgroundColor
        button.nonSelectedBorderColor = defaultBackgroundColor
        button.tag = index
        button.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
        return button
    }

    private func createBorder(index: Int) -> UIView {
        let border = UIView()
        border.backgroundColor = defaultBorderColor
        border.tag = index
        border.heightAnchor.constraint(equalToConstant: borderHeight).activate()
        return border
    }

    private func createSeparator() -> UIView {
        let separator = UIView()
        separator.backgroundColor = UIColor.lightGray
        separator.widthAnchor.constraint(equalToConstant: 1).activate()
        return separator
    }

    private func createSeparatorView(with button: UIButton) -> UIStackView {
        let separator = createSeparator()

        let stackView = UIStackView(arrangedSubviews: [button, separator])
        stackView.axis = .horizontal
        stackView.alignment = .center

        separator.heightAnchor.constraint(equalTo: stackView.heightAnchor, multiplier: 0.6).isActive = true

        return stackView
    }

    @objc private func didTapButton(_ sender: UIButton) {
        let index = sender.tag
        setActive(at: index)
        delegate?.tabView(self, didSelectAt: index)
    }

    func setActive(at index: Int) {
        buttons.forEach { $0.isSelected = $0.tag == index }
        borders.forEach { $0.backgroundColor = $0.tag == index ? selectedBorderColor : defaultBorderColor }
    }
}

private extension NSLayoutConstraint {
    func activate() {
        isActive = true
        priority = UILayoutPriority(rawValue: 999)
    }
}
