//
//  MobileTableViewCell.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import UIKit

protocol MobileTableViewCellDelegate: class {
    func mobileTableViewCellDelegate(_ cell: MobileTableViewCell, didTapFavourite sender: UIButton, mobileId: Int, isSelected: Bool)
}

final class MobileTableViewCell: UITableViewCell {

    @IBOutlet private weak var thumbnail: UIImageView!
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var details: UILabel!
    @IBOutlet private weak var price: UILabel!
    @IBOutlet private weak var rating: UILabel!
    @IBOutlet private weak var favourite: UIButton!
    private var mobileId: Int?

    weak var delegate: MobileTableViewCellDelegate?

    func configure(mobile: Mobile, isFavourite: Bool) {
        thumbnail.setImage(url: mobile.thumbImageURL)
        name.text = mobile.name
        details.text = mobile.description
        price.text = "Price: \(mobile.price)"
        rating.text = "Rating: \(mobile.rating)"
        favourite.isSelected = isFavourite
        mobileId = mobile.id
    }
    
    @IBAction func didTapFavourite(_ sender: UIButton) {
        favourite.isSelected = !favourite.isSelected
        guard let mobileId = mobileId else { return }
        delegate?.mobileTableViewCellDelegate(self, didTapFavourite: sender, mobileId: mobileId, isSelected: favourite.isSelected)
    }
}
