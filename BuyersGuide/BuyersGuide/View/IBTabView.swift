//
//  IBTabView.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import UIKit

/// For Interface Builder
@IBDesignable final class IBTabView: UIView {

    @IBInspectable var menu1: String = ""
    @IBInspectable var menu2: String = ""
    @IBInspectable var menu3: String = ""
    @IBInspectable var menu4: String = ""
    @IBInspectable var menu5: String = ""

    #if TARGET_INTERFACE_BUILDER
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        loadView()
    }
    #else
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    #endif

    var view: TabView? {
        return subviews.first as? TabView
    }

    @IBOutlet private weak var delegate: AnyObject?

    override func awakeFromNib() {
        super.awakeFromNib()
        loadView()
        view?.delegate = delegate as? TabViewDelegate
    }

    private func loadView() {
        let titles = [menu1, menu2, menu3, menu4, menu5].filter { !$0.isEmpty }
        let view = TabView(titles: titles)
        view.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(view, at: 0)
        view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
}
