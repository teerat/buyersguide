//
//  DetailsViewController.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/21.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import UIKit

final class DetailsViewController: UIViewController, StoryboardInstantiatable {

    // MARK: IBOutlet
    @IBOutlet private weak var imagesStackView: UIStackView!
    @IBOutlet private weak var price: UILabel!
    @IBOutlet private weak var rating: UILabel!
    @IBOutlet private weak var details: UILabel!

    var imagesStackViewHeight: CGFloat = 0
    private var images: [Image] = [] {
        didSet {
            reloadImages()
        }
    }

    // MARK: Dependency
    private var mobile: Mobile!
    func inject(_ value: Mobile) {
        mobile = value
    }

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchImagesData()
        configureMobileData()
    }

    // MARK: Private Function
    private func fetchImagesData() {
        MobileApi().fetchMobilesImages(mobileId: mobile.id) { [weak self]  result in
            guard let self = self else { return }
            switch result {
            case .success(let images):
                self.images = images
            case .failure(let error):
                self.images = []
                let alert = UIAlertController(title: error.info.title , message: error.info.reason + error.info.recoverySuggestion, preferredStyle: .alert)
                alert.addAction(.init(title: "Try Again", style: .default, handler: { _ in
                    self.fetchImagesData()
                }))
                alert.addAction(.init(title: "Close", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    private func configureMobileData() {
        title = mobile.name
        details.text = mobile.description
        price.text = "Price: \(mobile.price)"
        rating.text = "Rating: \(mobile.rating)"
    }

    private func reloadImages() {
        imagesStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        images.forEach {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFit
            imageView.setImage(url: $0.url) { _ in
                let imageWidth = imageView.image!.size.width
                let imageHeight = imageView.image!.size.height
                let frameHeight = self.imagesStackView.frame.height
                let newFrameWidth = (imageWidth * frameHeight) / imageHeight
                let newFrameSize = CGSize(width: newFrameWidth, height: frameHeight)
                imageView.frame.size = newFrameSize
                imageView.layoutIfNeeded()
                let widthConstraint = NSLayoutConstraint(item: imageView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: newFrameWidth)
                imageView.addConstraint(widthConstraint)
            }
            imagesStackView.addArrangedSubview(imageView)
        }
    }
}
