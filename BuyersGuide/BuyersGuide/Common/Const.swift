//
//  Const.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import Foundation

public class Const {
    public enum Error: Swift.Error {
        case general
        case offline

        var info: (title: String, reason: String, recoverySuggestion: String) {
            switch self {
            case .general: return ("Error", "Something Went Wrong!", "Please Try Again Later.")
            case .offline: return ("Offline", "You seem to be offline.", "please check your network connection.")
            }
        }
    }
}
