//
//  Instantiatable.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/21.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import UIKit

//Instantiatable
protocol Instantiatable: Injectable {
    static func instantiate(with dependency: Dependency) -> Self
}

extension Instantiatable where Dependency == Void {
    static func instantiate() -> Self {
        return instantiate(with: ())
    }
}

//Injectable
protocol Injectable {
    associatedtype Dependency = Void
    func inject(_ value: Dependency)
}

extension Injectable where Dependency == Void {
    func inject(_ value: Dependency) {
        // no operation
    }
}

//StoryboardInstantiatable
enum StoryboardInstantiateType {
    case initial
    case identifier(String)
}

protocol StoryboardInstantiatable: Instantiatable {
    static var sourceStoryboard: UIStoryboard { get }
    static var instantiateType: StoryboardInstantiateType { get }
}

extension StoryboardInstantiatable where Self: NSObject {
    static var instantiateType: StoryboardInstantiateType {
        return .identifier(String(describing: self))
    }
}

extension StoryboardInstantiatable where Self: UIViewController {
    static var sourceStoryboard: UIStoryboard {
        return UIStoryboard(name: String(describing: self), bundle: Bundle(for: self))
    }

    static func instantiate(with dependency: Dependency) -> Self {
        let viewController: Self
        switch instantiateType {
        case .initial:
            viewController = sourceStoryboard.instantiateInitialViewController() as! Self
        case .identifier(let identifier):
            viewController = sourceStoryboard.instantiateViewController(withIdentifier: identifier) as! Self
        }

        viewController.inject(dependency)

        return viewController
    }
}
