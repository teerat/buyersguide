//
//  UIImageView+.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import UIKit
import Nuke

extension UIImageView {

    public func setImage(url: String, placeholder: UIImage? = #imageLiteral(resourceName: "imagePlaceholder"), completion: ((Bool) -> Void)? = nil) {

        guard let url = URL(string: url.urlCorrection) else {
            image = placeholder
            completion?(false)
            return
        }

        Nuke.loadImage(with: url,
                       options: ImageLoadingOptions(placeholder: placeholder,
                                                    transition: .fadeIn(duration: 0.2)),
                       into: self) { _, error in
                completion?(error == nil)
        }
    }
}

private extension String {
    var urlCorrection: String {
        if self.contains("http://") || self.contains("https://") {
            return self
        } else {
            return "http://\(self)"
        }
    }
}
