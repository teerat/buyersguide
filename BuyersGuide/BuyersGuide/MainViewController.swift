//
//  MainViewController.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import UIKit

final class MainViewController: UIViewController {

    // MARK: IBOutlet
    @IBOutlet private weak var tabView: IBTabView!
    @IBOutlet private weak var tableView: UITableView!

    // MARK: Tab
    private enum Tab: Int {
        case all
        case favourite
    }
    private var currentTab = Tab.all {
        didSet {
            tableView.reloadData()
        }
    }

    // MARK: Data
    var mobiles: [Mobile] = [] {
        didSet {
            reloadTable()
        }
    }
    lazy var favouriteMobiles: [Mobile] = mobiles.filter { favouriteList.contains($0.id) }
    var favouriteList: [Int] = UserDefaults.standard.object(for: .favouriteList) as? [Int] ?? [] {
        didSet {
            UserDefaults.standard.set(favouriteList, for: .favouriteList)
            UserDefaults.standard.synchronize()
            reloadTable()
        }
    }
    var currentMobiles: [Mobile] {
        switch currentTab {
        case .all: return mobiles
        case .favourite: return favouriteMobiles
        }
    }

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tabView.view?.delegate = self
        tableView.tableFooterView = UIView()

        fetchData()
    }

    // MARK: Private Function
    private func fetchData() {
        MobileApi().fetchMobiles { [weak self]  result in
            guard let self = self else { return }
            switch result {
            case .success(let mobiles):
                self.mobiles = mobiles
            case .failure(let error):
                self.mobiles = []
                let alert = UIAlertController(title: error.info.title , message: error.info.reason + error.info.recoverySuggestion, preferredStyle: .alert)
                alert.addAction(.init(title: "Try Again", style: .default, handler: { _ in
                    self.fetchData()
                }))
                alert.addAction(.init(title: "Close", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    private func reloadTable() {
        favouriteMobiles = mobiles.filter { favouriteList.contains($0.id) }
        tableView.reloadData()
    }

    // MARK: IBAction
    @IBAction func didTapSortButton(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Sort", message: nil, preferredStyle: .alert)
        alert.addAction(.init(title: "Price low to high", style: .default, handler: { _ in
            self.mobiles.sort { $0.price < $1.price }
        }))
        alert.addAction(.init(title: "Price high to low", style: .default, handler: { _ in
            self.mobiles.sort { $0.price > $1.price }
        }))
        alert.addAction(.init(title: "Rating", style: .default, handler: { _ in
            self.mobiles.sort { $0.rating > $1.rating }
        }))
        alert.addAction(.init(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {

    // MARK: UITableViewDelegate, UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentMobiles.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellIdentifier = "mobileTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MobileTableViewCell else {
            return UITableViewCell()
        }
        let mobile = currentMobiles[indexPath.row]
        cell.configure(mobile: mobile, isFavourite: favouriteList.contains(mobile.id))
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let mobile = currentMobiles[indexPath.row]
        let detailsViewController = DetailsViewController.instantiate(with: mobile)
        navigationController?.pushViewController(detailsViewController, animated: true)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return currentTab == .favourite
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let editedMobile = favouriteMobiles[indexPath.row]
        favouriteList = favouriteList.filter { $0 != editedMobile.id }
    }
}

extension MainViewController: MobileTableViewCellDelegate {

    // MARK: MobileTableViewCellDelegate
    func mobileTableViewCellDelegate(_ cell: MobileTableViewCell, didTapFavourite sender: UIButton, mobileId: Int, isSelected: Bool) {
        if isSelected {
            favouriteList.append(mobileId)
        } else {
            favouriteList = favouriteList.filter { $0 != mobileId }
        }
    }
}

extension MainViewController: TabViewDelegate {

    // MARK: TabViewDelegate
    func tabView(_ tabView: TabView, didSelectAt index: Int) {
        guard let selectedTab = Tab(rawValue: index) else { return }
        currentTab = selectedTab
    }
}
