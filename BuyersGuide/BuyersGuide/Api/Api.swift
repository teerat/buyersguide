//
//  Api.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import Foundation
import Alamofire

class Api {

    internal func sendRequest<T: Decodable>(url: String, completion: @escaping (Result<T>) -> Void) {

        guard NetworkReachabilityManager()?.isReachable != false else {
            completion(.failure(Const.Error.offline))
            return
        }

        SessionManager.default.request(url).responseData { result in
            guard let data = result.data else {
                completion(.failure(Const.Error.general))
                return
            }

            let decoder = JSONDecoder()
            do {
                let typedData = try decoder.decode(T.self, from: data)
                completion(.success(typedData))
            } catch {
                completion(.failure(Const.Error.general))
            }
        }
    }
}
