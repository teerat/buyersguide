//
//  Image.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import Foundation

struct Image: Codable {
    let url: String
    let id: Int
    let mobile_id: Int
}
