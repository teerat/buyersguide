//
//  Result.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import Foundation

public protocol ResultType {
    associatedtype Value
    associatedtype Error

    var isSuccess: Bool { get }
    var isFailure: Bool { get }

    var value: Value? { get }
    var error: Error? { get }
}

public enum Result<T>: ResultType {
    case success(T)
    case failure(Const.Error)

    public var isSuccess: Bool {
        return value != nil
    }

    public var isFailure: Bool {
        return !isSuccess
    }

    public var value: T? {
        if case .success(let value) = self {
            return value
        }
        return nil
    }

    public var error: Error? {
        if case .failure(let error) = self {
            return error
        }
        return nil
    }

    public func map<U>(_ transform: (Value) -> U) -> Result<U> {
        if let value = value {
            return .success(transform(value))
        } else if error != nil {
            return .failure(Const.Error.general)
        } else {
            fatalError("not reachable")
        }
    }
}
