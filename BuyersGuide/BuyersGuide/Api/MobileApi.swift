//
//  MobileApi.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import Foundation

final class MobileApi: Api {

    public func fetchMobiles(completion: @escaping (Result<[Mobile]>) -> Void) {
        let mobilesUrlString = "https://scb-test-mobile.herokuapp.com/api/mobiles/"
        sendRequest(url: mobilesUrlString, completion: completion)
    }

    public func fetchMobilesImages(mobileId: Int, completion: @escaping (Result<[Image]>) -> Void) {
         let mobilesImagesUrlString = "https://scb-test-mobile.herokuapp.com/api/mobiles/\(mobileId)/images/"
        sendRequest(url: mobilesImagesUrlString, completion: completion)
    }
}
