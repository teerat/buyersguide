//
//  Mobile.swift
//  BuyersGuide
//
//  Created by Namchaisawatwong Teerat on 2019/04/20.
//  Copyright © 2019 Namchaisawatwong Teerat. All rights reserved.
//

import Foundation

struct Mobile: Codable {
    let brand: String
    let name: String
    let thumbImageURL: String
    let rating: Double
    let description: String
    let price: Double
    let id: Int
}
